const middleware = {}

middleware['auth-admin'] = require('../middleware/auth-admin.js')
middleware['auth-admin'] = middleware['auth-admin'].default || middleware['auth-admin']

middleware['check-resetToken'] = require('../middleware/check-resetToken.js')
middleware['check-resetToken'] = middleware['check-resetToken'].default || middleware['check-resetToken']

export default middleware
