import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _72ffa5c4 = () => interopDefault(import('../pages/about/index.vue' /* webpackChunkName: "pages/about/index" */))
const _58f47b40 = () => interopDefault(import('../pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */))
const _78a797a2 = () => interopDefault(import('../pages/contactUs/index.vue' /* webpackChunkName: "pages/contactUs/index" */))
const _bc3c4dee = () => interopDefault(import('../pages/products/index.vue' /* webpackChunkName: "pages/products/index" */))
const _7967b0fa = () => interopDefault(import('../pages/services/index.vue' /* webpackChunkName: "pages/services/index" */))
const _02aa3ef2 = () => interopDefault(import('../pages/admin/enquiries/index.vue' /* webpackChunkName: "pages/admin/enquiries/index" */))
const _ac0f7876 = () => interopDefault(import('../pages/admin/forgot_password.vue' /* webpackChunkName: "pages/admin/forgot_password" */))
const _60ab7b04 = () => interopDefault(import('../pages/admin/home/index.vue' /* webpackChunkName: "pages/admin/home/index" */))
const _5a164049 = () => interopDefault(import('../pages/admin/products/index.vue' /* webpackChunkName: "pages/admin/products/index" */))
const _1b33090c = () => interopDefault(import('../pages/admin/profile/index.vue' /* webpackChunkName: "pages/admin/profile/index" */))
const _280a416d = () => interopDefault(import('../pages/admin/reset_password.vue' /* webpackChunkName: "pages/admin/reset_password" */))
const _7b808ec3 = () => interopDefault(import('../pages/admin/services/index.vue' /* webpackChunkName: "pages/admin/services/index" */))
const _32a54774 = () => interopDefault(import('../pages/products/_id/index.vue' /* webpackChunkName: "pages/products/_id/index" */))
const _3e206e24 = () => interopDefault(import('../pages/services/_id/index.vue' /* webpackChunkName: "pages/services/_id/index" */))
const _9d315cc0 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _72ffa5c4,
    name: "about"
  }, {
    path: "/admin",
    component: _58f47b40,
    name: "admin"
  }, {
    path: "/contactUs",
    component: _78a797a2,
    name: "contactUs"
  }, {
    path: "/products",
    component: _bc3c4dee,
    name: "products"
  }, {
    path: "/services",
    component: _7967b0fa,
    name: "services"
  }, {
    path: "/admin/enquiries",
    component: _02aa3ef2,
    name: "admin-enquiries"
  }, {
    path: "/admin/forgot_password",
    component: _ac0f7876,
    name: "admin-forgot_password"
  }, {
    path: "/admin/home",
    component: _60ab7b04,
    name: "admin-home"
  }, {
    path: "/admin/products",
    component: _5a164049,
    name: "admin-products"
  }, {
    path: "/admin/profile",
    component: _1b33090c,
    name: "admin-profile"
  }, {
    path: "/admin/reset_password",
    component: _280a416d,
    name: "admin-reset_password"
  }, {
    path: "/admin/services",
    component: _7b808ec3,
    name: "admin-services"
  }, {
    path: "/products/:id",
    component: _32a54774,
    name: "products-id"
  }, {
    path: "/services/:id",
    component: _3e206e24,
    name: "services-id"
  }, {
    path: "/",
    component: _9d315cc0,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
