import colors from "vuetify/es5/util/colors";

export default {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: "%s - " + "BAMBUT",
    title: "BAMBUT" || "",
    meta: [{
        charset: "utf-8",
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1",
      },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || "",
      },
    ],
    link: [{
      rel: "icon",
      type: "image/x-icon",
      href: "/favicon.ico",
    }, ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: "#ff3167",
    duration: 7000,
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{
      src: "~/plugins/localStorage.js",
      ssr: false,
    },
    '~/plugins/core-components.js'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/eslint-module",
    "@nuxtjs/vuetify",
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    "@nuxtjs/auth",
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.BASE_URL || "http://localhost:3000/api",
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    treeshake: true,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
        light: {
          main: "#ff3167",
          main2: "#906feb",
          back: "#fafafa",
        },
      },
    },
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
  env: {
    BASE_URL: process.env.BASE_URL || "http://localhost:3000/api",
    PHOTO_URL: process.env.PHOTO_URL || "http://localhost:3000/image/",
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/appUsers/login",
            method: "post",
            propertyName: "id",
          },
          logout: {
            url: "/appUsers/logout",
            method: "post",
          },
          user: false,
          // user: {
          //   url: `/appUsers/${}`,
          //   method: 'get',
          //   propertyName: 'user'
          // }
        },
        // tokenRequired: true,
        tokenType: "",
        // globalToken: true,
        // autoFetchUser: true
      },
    },
  },
};