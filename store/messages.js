export const state = () => ({
    messages: [],
    newCount: 0,
    enqGraph: []
})
export const mutations = {
    setMessages(state, data) {
        state.messages = data
        state.newCount = data.filter(item => item.new === true).length

        let thisYear = new Date().getFullYear();
        let newEnq = data.filter(item => new Date(item.logTime).getFullYear() === thisYear);
        let monthArr = [];

        if (newEnq.length > 0) {
            for (let i = 0; i < 12; i++) {
                let count = 0;
                newEnq.forEach(item => {
                    if (new Date(item.logTime).getMonth() === i) {
                        count++
                    }
                })
                monthArr[i] = count;
            }
            state.enqGraph = monthArr;
        } else {
            state.enqGraph = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }
    },
    deleteMessage(state, id) {
        let index = state.messages.findIndex(item => item.id === id);
        state.messages.splice(index, 1);
    },
    readMessages(state) {
        state.newCount = 0
    }
}
export const actions = {
    async getMessages({
        commit
    }) {
        await this.$axios.get('/messages').then(res => {
            if (res.status === 200) {
                commit('setMessages', res.data)
            }
        })
    },
    async deleteMessage({
        commit
    }, id) {
        if (confirm("Are you sure you want to delete this?")) {
            await this.$axios.delete(`/messages/${id}`).then(res => {
                if (res.status === 200) {
                    commit('deleteMessage', id);
                }
            })
        }

    },
    async readMessages({
        commit
    }) {
        await this.$axios.post('/messages/update?where={}', {
            new: false
        }).then(res => {
            if (res.status === 200) {
                commit('readMessages');
            }

        }).catch(err => console.log(err))
    },
    async sendMessage({
        commit
    }, message) {
        try {
            let res = await this.$axios.post('/messages', message);

        } catch (err) {
            console.log(err);
        }
    }
}

export const getters = {
    loadMessages(state) {
        return state.messages
    }
}