export const state = () => ({
  products: [],
  services: [],
  prodGraph: [],
  servGraph: []
});

export const mutations = {
  setProd(state, items) {
    state.products = items
    let thisYear = new Date().getFullYear();
    let newProds = items.filter(item => new Date(item.logTime).getFullYear() === thisYear);
    let monthArr = [];

    if (newProds.length > 0) {
      for (let i = 0; i < 12; i++) {
        let count = 0;
        newProds.forEach(item => {
          if (new Date(item.logTime).getMonth() === i) {
            count++
          }
        })
        monthArr[i] = count;
      }
      state.prodGraph = monthArr;
    } else {
      state.prodGraph = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
  },
  setServ(state, items) {
    state.services = items
    let thisYear = new Date().getFullYear();
    let newServs = items.filter(item => new Date(item.logTime).getFullYear() === thisYear);
    let monthArr = [];

    if (newServs.length > 0) {
      for (let i = 0; i < 12; i++) {
        let count = 0;
        newServs.forEach(item => {
          if (new Date(item.logTime).getMonth() === i) {
            count++
          }
        })
        monthArr[i] = count;
      }
      state.servGraph = monthArr;
    } else {
      state.servGraph = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
  },
  addProd(state, item) {
    state.products.push(item)
  },
  addServ(state, item) {
    state.services.push(item)
  },
  editProd(state, item) {
    let id = item.id
    let index = state.products.findIndex((item) => item.id === id)

    state.products.splice(index, 1, item);

  },
  editServ(state, item) {
    let id = item.id
    let index = state.services.findIndex((item) => item.id === id)

    state.services.splice(index, 1, item);
  },
  deleteProd(state, item) {
    let id = item.id
    let idx = state.products.findIndex((item) => item.id === id);
    state.products.splice(idx, 1)
  },
  deleteServ(state, item) {
    let id = item.id
    let idx = state.services.findIndex((item) => item.id === id);
    state.services.splice(idx, 1)
  }
};

export const actions = {

  async getItems({
    commit
  }, info) {


    let url = ""
    if (info === "prod") {
      url = "products"
    } else if (info === "serv") {
      url = "services"
    }

    try {
      let res = await this.$axios.get(`/${url}`)

      if (res.status === 200 && info === "prod") {
        commit('setProd', res.data)
      } else if (res.status === 200 && info === "serv") {
        commit('setServ', res.data)
      }
    } catch (err) {
      console.log(err)
    }
  },
  async setItem({
    commit
  }, {
    data,
    files
  }) {

    //console.log("wey gud wegen", data);

    let id = data.id
    let url = ""
    let method = "post"
    let info = data.info

    if (data.info === "prod") {
      url = "products"
      if (id) {
        url = `products/${id}`
        method = "patch"
        delete data.id
      }
    } else if (data.info === "serv") {
      url = "services"
      if (id) {
        url = `services/${id}`
        method = "patch"
        delete data.id
      }
    }

    delete data.info
    let formData;

    if (files) {
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }
    }

    // save the record

    try {

      if (files) {
        let res = await this.$axios.post('/myfiles/image/upload', formData)
        data.photos = res.data.result.files.files.map(item => {
          return item.name
        })
      }

      let response = await this.$axios[method](
        url,
        data
      )
      if (response.data && response.data.id) {
        if (info === "prod") {

          if (id) {
            commit('editProd', response.data);
          } else {
            // add the new item 
            commit('addProd', response.data)
          }
        } else if (info === "serv") {
          if (id) {
            commit('editServ', response.data);
          } else {
            // add the new item 
            commit('addServ', response.data)
          }
        }
      }
    } catch (err) {
      console.log(err);

    }
  },
  async deleteItem({
    commit
  }, item) {
    let url = ""
    let info = item.info
    let pics = item.photos

    if (info === "prod") {
      url = `/products/${item.id}`
    } else if (info === "serv") {
      url = `/services/${item.id}`
    }

    delete item.info

    if (confirm("Are you sure you want to delete this?")) {

      try {
        await this.$axios
          .delete(url)
        if (info === "prod") {
          commit('deleteProd', item)
        } else if (info === "serv") {
          commit('deleteServ', item)
        }

        if (pics) {
          if (pics.length !== 0) {
            pics.forEach(async picName => {
              await await this.$axios.delete(`/myfiles/image/files/${picName}`)
              console.log("deleted!");

            })
          }
        }

      } catch (err) {
        console.log(err);

      }
    }
  },
  async deletePhoto({
    dispatch
  }, {
    data,
    picName
  }) {

    try {
      if (confirm("Are you sure you want to delete this?")) {
        await this.$axios.delete(`/myfiles/image/files/${picName}`)


        let dd = Object.assign({}, data);
        let photoIdx = dd.photos.findIndex(name => name === picName);
        dd.photos.splice(photoIdx, 1);

        dispatch('setItem', {
          data: dd,
          files: null
        })
      }
    } catch (err) {
      console.log(err);
    }

  },

  async addNewPic({
    dispatch
  }, {
    data,
    files
  }) {

    try {
      let dd = Object.assign({}, data);

      let formData;
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }

      let res = await this.$axios.post('/myfiles/image/upload', formData)
      let picNames = res.data.result.files.files.map(item => {
        return item.name
      })

      if (dd.photos.length === 0) {
        dd.photos = picNames
      } else if (dd.photos.length > 0) {
        picNames.forEach(picName => dd.photos.push(picName))
      }

      dispatch('setItem', {
        data: dd,
        files: null
      })
    } catch (err) {
      console.log(err);
    }

  }
};

export const getters = {
  loadedProd(state) {
    return state.products;
  },
  loadedServ(state) {
    return state.services;
  },
  topProds(state) {
    let prodDates = state.products.map((item) => {
      return new Date(item.logTime).getTime()
    });

    prodDates.sort(function (a, b) {
      return b - a
    });
    let topThreeDates = prodDates.slice(0, 3);
    let topThreeProducts = topThreeDates.map(item => {
      for (let i = 0; i < state.products.length; i++) {
        if (item === new Date(state.products[i].logTime).getTime()) {
          return state.products[i];
        }
      }
    })


    return topThreeProducts;

  }
}