import vue from "vue";

import upFooter from '@/components/upFooter';
import bamAddr from "@/components/bamAddr";
import singleItem from "@/components/singleItem";

vue.component('upFooter', upFooter);
vue.component('bamAddr', bamAddr);
vue.component('singleItem', singleItem);